﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "KillCount")]
public class KillCount : ScriptableObject
{   
    [HideInInspector]
    public int killCount;   
}
