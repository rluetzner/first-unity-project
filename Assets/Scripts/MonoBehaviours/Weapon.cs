﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(KillCounter))]
public class Weapon : MonoBehaviour
{
    public GameObject ammoPrefab;
    static List<GameObject> ammoPool;
    public int poolSize;
    public float weaponVelocity;

    bool isFiring;

    [HideInInspector]
    public Animator animator;

    public AudioClip fireSfx;
    AudioSource audioSource;

    Camera localCamera;

    float positiveSlope;
    float negativeSlope;

    KillCounter killCounter;

    enum Quadrant
    {
        East,
        South,
        West,
        North
    }

    private void Awake()
    {
        if (ammoPool == null)
        {
            ammoPool = new List<GameObject>();
            for (int i = 0; i < poolSize; i++)
            {
                var ammoObject = Instantiate(ammoPrefab);
                ammoObject.SetActive(false);
                ammoPool.Add(ammoObject);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        killCounter = GetComponent<KillCounter>();
        isFiring = false;
        localCamera = Camera.main;
        var lowerLeft = localCamera.ScreenToWorldPoint(new Vector2(0, 0));
        var upperRight = localCamera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        var upperLeft = localCamera.ScreenToWorldPoint(new Vector2(0, Screen.height));
        var lowerRight = localCamera.ScreenToWorldPoint(new Vector2(Screen.width, 0));

        positiveSlope = GetSlope(lowerLeft, upperRight);
        negativeSlope = GetSlope(upperLeft, lowerRight);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isFiring = true;
            FireAmmo();
        }
        UpdateState();
    }

    void UpdateState()
    {
        if (isFiring)
        {
            Vector2 quadrantVector;
            var quadEnum = GetQuadrant();
            switch (quadEnum)
            {
                case Quadrant.East:
                    quadrantVector = new Vector2(1.0f, 0.0f);
                    break;
                case Quadrant.South:
                    quadrantVector = new Vector2(0.0f, -1.0f);
                    break;
                case Quadrant.West:
                    quadrantVector = new Vector2(-1.0f, 0.0f);
                    break;
                case Quadrant.North:
                    quadrantVector = new Vector2(0.0f, 1.0f);
                    break;
                default:
                    quadrantVector = new Vector2(0.0f, 0.0f);
                    break;
            }
            animator.SetBool("isFiring", true);
            animator.SetFloat("fireXDir", quadrantVector.x);
            animator.SetFloat("fireYDir", quadrantVector.y);
            isFiring = false;
        }
        else
        {
            animator.SetBool("isFiring", false);
        }
    }

    bool HigherThanPositiveSlopeLine(Vector2 inputPosition)
    {
        return higherThanSlope(inputPosition, positiveSlope);
    }

    private bool higherThanSlope(Vector2 inputPosition, float slope)
    {
        var playerPosition = gameObject.transform.position;
        var mousePosition = localCamera.ScreenToWorldPoint(inputPosition);
        var yIntercept = playerPosition.y - (slope * playerPosition.x);
        var inputIntercept = mousePosition.y - (slope * mousePosition.x);
        return inputIntercept > yIntercept;
    }

    bool HigherThanNegativeSlopeLine(Vector2 inputPosition)
    {
        return higherThanSlope(inputPosition, negativeSlope);
    }

    float GetSlope(Vector2 pointOne, Vector2 pointTwo)
    {
        return (pointTwo.y - pointOne.y) / (pointTwo.x - pointOne.x);
    }

    Quadrant GetQuadrant()
    {
        var mousePosition = Input.mousePosition;
        var playerPosition = transform.position;
        var higherThanPositiveSlopeLine = HigherThanPositiveSlopeLine(mousePosition);
        var higherThanNegativeSlopeLine = HigherThanNegativeSlopeLine(mousePosition);
        if (!higherThanPositiveSlopeLine && higherThanNegativeSlopeLine)
        {
            return Quadrant.East;
        }
        else if (!higherThanPositiveSlopeLine && !higherThanNegativeSlopeLine)
        {
            return Quadrant.South;
        }
        else if (higherThanPositiveSlopeLine && higherThanNegativeSlopeLine)
        {
            return Quadrant.North;
        }
        else
        {
            return Quadrant.West;
        }
    }

    GameObject SpawnAmmo(Vector3 location)
    {
        var ammo = ammoPool.FirstOrDefault(a => !a.activeSelf);
        if (ammo == null) return null;
        ammo.SetActive(true);
        ammo.transform.position = location;
        var ammoScript = ammo.GetComponent<Ammo>();
        ammoScript.killCounter = killCounter;
        return ammo;
    }

    void FireAmmo()
    {
        var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var ammo = SpawnAmmo(transform.position);
        if (ammo != null)
        {
            if (fireSfx != null)
            {
                audioSource.PlayOneShot(fireSfx);
            }
            var arcScript = ammo.GetComponent<Arc>();
            var travelDuration = 1.0f / weaponVelocity;
            StartCoroutine(arcScript.TravelArc(mousePosition, travelDuration));
        }
    }

    private void OnDestroy()
    {
        ammoPool = null;
    }
}
