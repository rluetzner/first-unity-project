﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class KillCounter : MonoBehaviour
{
    public float sequentialKillTimeout;

    public KillCount killCount;
    Coroutine resetSequentialKills;

    public AudioClip fiveKillsInARow;
    public AudioClip tenKillsInARow;
    public AudioClip fifteenKillsInARow;
    public AudioClip twentyKillsInARow;
    public AudioClip twentyFiveKillsInARow;

    AudioSource audioSource;
    int sequentialKills = 0;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void IncrementKillCount()
    {
        if (resetSequentialKills != null)
        {
            StopCoroutine(resetSequentialKills);
        }
        killCount.killCount++;
        sequentialKills++;
        PlaySfx();
        resetSequentialKills = StartCoroutine(ResetSequentialKills());
        print($"Current Kills:{killCount.killCount}");
    }

    private void PlaySfx()
    {
        switch (sequentialKills)
        {
            case 5:
                if (fiveKillsInARow != null) audioSource.PlayOneShot(fiveKillsInARow);
                break;
            case 10:
                if (tenKillsInARow != null) audioSource.PlayOneShot(tenKillsInARow);
                break;
            case 15:
                if (fifteenKillsInARow != null) audioSource.PlayOneShot(fifteenKillsInARow);
                break;
            case 20:
                if (twentyKillsInARow != null) audioSource.PlayOneShot(twentyKillsInARow);
                break;
            case 25:
                if (twentyFiveKillsInARow != null) audioSource.PlayOneShot(twentyFiveKillsInARow);
                break;
            default:
                break;
        }
    }

    private IEnumerator ResetSequentialKills()
    {
        yield return new WaitForSeconds(sequentialKillTimeout);
        sequentialKills = 0;
    }    
}
