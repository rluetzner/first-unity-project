﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    public int damageStrength;
    Coroutine damageCoroutine;

    float hitPoints;

    public float lootDropChance;
    public GameObject[] potentialLootDropPrefabs;

    public virtual IEnumerator DamageCharacter(int damage, float interval, KillCounter killCounter)
    {
        while (true)
        {
            StartCoroutine(FlickerCharacter());
            hitPoints -= damage;
            if (hitPoints <= float.Epsilon)
            {
                if (killCounter != null)
                {
                    killCounter.IncrementKillCount();
                }
                KillCharacter();
                break;
            }

            if (interval > float.Epsilon)
            {
                yield return new WaitForSeconds(interval);
            }
            else
            {
                break;
            }
        }
    }

    public override IEnumerator DamageCharacter(int damage, float interval)
    {
        return DamageCharacter(damage, interval, null);
    }

    public override void KillCharacter()
    {
        base.KillCharacter();
        var rng = Random.Range(0.0f, 1.0f);
        if (rng <= lootDropChance)
        {
            var objIdx = Random.Range(0, potentialLootDropPrefabs.Length);
            var obj = Instantiate(potentialLootDropPrefabs[objIdx]);
            obj.transform.position = transform.position;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            var player = collision.gameObject.GetComponent<Player>();

            if (damageCoroutine == null)
            {
                damageCoroutine = StartCoroutine(player.DamageCharacter(damageStrength, 1.0f));
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (damageCoroutine != null)
            {
                StopCoroutine(damageCoroutine);
                damageCoroutine = null;
            }
        }
    }

    public override void ResetCharacter()
    {
        hitPoints = startingHitPoints;
    }

    private void OnEnable()
    {
        ResetCharacter();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
