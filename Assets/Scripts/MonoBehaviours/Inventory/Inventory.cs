﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public GameObject slotPrefab;
    public const int numSlots = 5;
    Image[] itemImages = new Image[numSlots];
    Item[] items = new Item[numSlots];
    GameObject[] slots = new GameObject[numSlots];

    public void CreateSlots()
    {
        if(slotPrefab != null)
        {
            for (int i = 0; i < numSlots; i++)
            {
                var newSlot = Instantiate(slotPrefab);
                newSlot.name = $"ItemSlot_{i}";
                newSlot.transform.SetParent(gameObject.transform.GetChild(0).transform);
                slots[i] = newSlot;
                itemImages[i] = newSlot.transform.GetChild(1).GetComponent<Image>();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CreateSlots();
    }

    public bool AddItem(Item itemToAdd)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] != null && items[i].itemType == itemToAdd.itemType && itemToAdd.stackable)
            {
                items[i].quantity += itemToAdd.quantity;
                var slotScripts = slots[i].gameObject.GetComponent<Slot>();
                var quantityText = slotScripts.qtyText;
                quantityText.enabled = true;
                quantityText.text = items[i].quantity.ToString();
                return true;
            }
            if (items[i] == null)
            {
                items[i] = Instantiate(itemToAdd);
                items[i].quantity = itemToAdd.quantity;
                itemImages[i].sprite = itemToAdd.sprite;
                itemImages[i].enabled = true;
                var slotScripts = slots[i].gameObject.GetComponent<Slot>();
                var quantityText = slotScripts.qtyText;
                quantityText.enabled = true;
                quantityText.text = items[i].quantity.ToString();
                return true;
            }
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
