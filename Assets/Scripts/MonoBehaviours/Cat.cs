﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Cat : Enemy
{
    AudioSource audioSource;

    public AudioClip[] catHurtSfx;
    public AudioClip catDiedSfx;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public override IEnumerator DamageCharacter(int damage, float interval, KillCounter killCounter)
    {
        PlayHurtSound();
        return base.DamageCharacter(damage, interval, killCounter);
    }

    public override IEnumerator DamageCharacter(int damage, float interval)
    {
        return DamageCharacter(damage, interval, null);
    }

    private void PlayHurtSound()
    {
        var rngIdx = Random.Range(0, catHurtSfx.Length);
        var sound = catHurtSfx[rngIdx];
        if (sound != null)
        {
            audioSource.PlayOneShot(catHurtSfx[rngIdx]);
        }
    }

    public override void KillCharacter()
    {
        if (catHurtSfx != null) audioSource.PlayOneShot(catDiedSfx);
        base.KillCharacter();
    }
}
