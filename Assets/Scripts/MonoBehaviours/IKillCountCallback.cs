﻿public interface IKillCountCallback
{
    void IncrementedKillCount(int currentKillCount);
    void IncrementedSequentialKillCount(int currentKillCount);
}