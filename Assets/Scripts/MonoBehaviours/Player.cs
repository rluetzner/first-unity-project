﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Player : Character
{
    string consumable = "CanBePickedUp";

    public HitPoints hitPoints;

    public HealthBar healthBarPrefab;
    HealthBar healthBar;

    public Inventory inventoryPrefab;
    Inventory inventory;

    public KillCountDisplay killCountDisplayPrefab;
    KillCountDisplay killCountDisplay;

    public KillCount killCount;
       
    bool isDead = false;

    Animator animator;

    public AudioClip playerHurt;
    public AudioClip playerHeal;
    public AudioClip playerDied;
    AudioSource audioSource;

    private void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        ResetCharacter();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(consumable))
        {
            var hitObject = collision.gameObject.GetComponent<Consumable>().item;
            if (hitObject != null)
            {
                bool shouldDisappear = false;
                switch (hitObject.itemType)
                {
                    case Item.ItemType.COIN:
                        shouldDisappear = inventory.AddItem(hitObject);
                        break;
                    case Item.ItemType.HEALTH:
                        shouldDisappear = AdjustHitPoints(hitObject.quantity);
                        break;
                    default:
                        break;
                }                
                if (shouldDisappear) collision.gameObject.SetActive(false);
            }            
        }
    }

    bool AdjustHitPoints(int amount)
    {
        if (hitPoints.value < maxHitPoints)
        {
            hitPoints.value = Mathf.Min(maxHitPoints, hitPoints.value + amount);
            print($"Adjusted hitpoints by: {amount}. New value: {hitPoints}");
            return true;
        }
        return false;
    }

    public override void ResetCharacter()
    {
        inventory = Instantiate(inventoryPrefab);
        hitPoints.value = startingHitPoints;
        healthBar = Instantiate(healthBarPrefab);
        healthBar.character = this;
        killCountDisplay = Instantiate(killCountDisplayPrefab);
        killCount.killCount = 0;   
    }

    public override void KillCharacter()
    {
        isDead = true;
        if (playerDied != null)
        {
            audioSource.PlayOneShot(playerDied);
        }
        Destroy(healthBar.gameObject);
        Destroy(inventory.gameObject);
        Destroy(killCountDisplay.gameObject);
        animator.SetBool("isDead", true);
    }

    public override IEnumerator DamageCharacter(int damage, float interval)
    {
        while (true)
        {
            StartCoroutine(FlickerCharacter());
            if (playerHurt != null)
            {
                audioSource.PlayOneShot(playerHurt);
            }
            hitPoints.value -= damage;
            if (hitPoints.value <= float.Epsilon)
            {
                KillCharacter();
                break;
            }

            if (interval > float.Epsilon)
            {
                yield return new WaitForSeconds(interval);
            }
            else
            {
                break;
            }
        }
    }
}
