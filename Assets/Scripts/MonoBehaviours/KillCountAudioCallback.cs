﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class KillCountAudioCallback : MonoBehaviour, IKillCountCallback
{
    public AudioClip fiveKillsInARow;
    public AudioClip tenKillsInARow;
    public AudioClip fifteenKillsInARow;
    public AudioClip twentyKillsInARow;
    public AudioClip twentyFiveKillsInARow;

    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void IncrementedKillCount(int currentKillCount)
    {
        throw new System.NotImplementedException();
    }

    public void IncrementedSequentialKillCount(int currentKillCount)
    {
        switch (currentKillCount)
        {
            case 5:
                if (fiveKillsInARow != null) audioSource.PlayOneShot(fiveKillsInARow);
                break;
            case 10:
                if (tenKillsInARow != null) audioSource.PlayOneShot(tenKillsInARow);
                break;
            case 15:
                if (fifteenKillsInARow != null) audioSource.PlayOneShot(fifteenKillsInARow);
                break;
            case 20:
                if (twentyKillsInARow != null) audioSource.PlayOneShot(twentyKillsInARow);
                break;
            case 25:
                if (twentyFiveKillsInARow != null) audioSource.PlayOneShot(twentyFiveKillsInARow);
                break;
            default:
                break;
        }
    }
}